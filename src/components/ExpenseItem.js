import './ExpenseItem.css'; 

function ExpenseItem() {
    const expenseDate = new Date().toDateString();
    const expenseTitle = "Cat Insurance";
    const expensePrice = "$320.2";

    return (
        <div className="expense-item">
            <div>
                {expenseDate}
            </div>
            <div className="expense-item__description">
                <h2>{expenseTitle}</h2>
                <div className="expense-item__price">{expensePrice}</div>
            </div>
        </div>
    )
}

export default ExpenseItem;